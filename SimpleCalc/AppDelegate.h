//
//  AppDelegate.h
//  SimpleCalc
//
//  Created by Guillem Fernandez on 18/10/2012.
//  Copyright (c) 2012 Guillem Fernandez. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
